#![allow(unused)]
mod database;
mod error;
mod ui;
use iced::{
    event::{self, Event},
    widget::{column, container, pick_list, row, text},
    window, Application, Command, Element, Font, Length, Settings, Subscription, Theme,
};
use ui::app_settings::{AppSettings, AppSettingsMessage};
use ui::edit::{Edit, EditMessage};
use ui::home::{Home, HomeMessage};
use ui::practice::{Practice, PracticeMessage};

const HEADER_SIZE: u16 = 32;
const TAB_PADDING: u16 = 16;
const ICON: Font = Font::with_name("icons");
const WINDOW_SIZE: (u32, u32) = (500, 700);

enum Icon {
    User,
    Heart,
    Calc,
    CogAlt,
}

impl From<Icon> for char {
    fn from(icon: Icon) -> Self {
        match icon {
            Icon::User => '\u{E800}',
            Icon::Heart => '\u{E801}',
            Icon::Calc => '\u{F1EC}',
            Icon::CogAlt => '\u{E802}',
        }
    }
}
use database::database_actor_handler::DatabaseActorHandler;
use database::{DatabaseActor, DatabaseActorMessage, DatabaseWorker};
use once_cell::sync::OnceCell;
use std::sync::mpsc;
use std::sync::Arc;
use std::thread;

static DB: OnceCell<Arc<DatabaseActorHandler>> = OnceCell::new();

#[tokio::main]
async fn main() -> iced::Result {
    let (sender, receiver) = mpsc::channel::<DatabaseActorMessage>();
    let handler = DatabaseActorHandler::new(sender);
    DB.set(Arc::new(handler))
        .expect("unable to set global DB handler");
    let database_thread = thread::spawn(|| {
        let connection = database::connection().unwrap();

        let worker = DatabaseWorker::new(&connection);
        let mut actor = DatabaseActor::new(receiver, worker);
        actor.run();
    });

    let iced_result = App::run(Settings {
        window: window::Settings {
            // position: iced::window::Position::Specific(100, 100),
            size: WINDOW_SIZE,
            min_size: Some((430, 600)),
            ..window::Settings::default()
        },
        ..Settings::default()
    });
    database_thread.join();
    iced_result
}

struct App {
    active_tab: Page,
    home: Home,
    edit: Edit,
    practice: Practice,
    app_settings: AppSettings,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Page {
    Home,
    Edit,
    Practice,
    AppSettings,
}
impl Page {
    const ALL: [Page; 4] = [Page::Home, Page::Edit, Page::Practice, Page::AppSettings];
}

impl std::fmt::Display for Page {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Page::Home => "Home",
                Page::Edit => "Edit",
                Page::Practice => "Practice",
                Page::AppSettings => "Settings",
            }
        )
    }
}

#[derive(Clone, Debug)]
pub enum Message {
    Home(HomeMessage),
    PageSelected(Page),
    WindowResized(u32, u32),
}

impl Application for App {
    type Message = Message;
    type Theme = Theme;
    type Executor = iced::executor::Default;
    type Flags = ();

    fn new(_flags: ()) -> (App, Command<Message>) {
        use std::fs;
        use std::path::Path;
        // TODO: think on error reportings in ui??
        let working_dir = std::env::var("CARGO_MANIFEST_DIR").unwrap();
        let path = Path::new(&working_dir);
        let path = path.join("fonts/icons.ttf");
        let path = path.to_str().unwrap();
        let icon_bytes: &[u8] = fs::read(path).unwrap().as_slice();
        (
            // TODO: new returns tuple
            App {
                active_tab: Page::Home,
                home: Home::new(()).0,
                edit: Edit::new(()).0,
                practice: Practice::new(()).0,
                app_settings: AppSettings::new(()).0,
            },
            Command::none(),
        )
    }

    fn title(&self) -> String {
        String::from("Words")
    }

    fn update(&mut self, message: Self::Message) -> Command<Message> {
        match message {
            Message::PageSelected(p) => match p {
                Page::Home => self.active_tab = Page::Home,
                Page::Edit => self.active_tab = Page::Edit,
                Page::Practice => self.active_tab = Page::Practice,
                Page::AppSettings => self.active_tab = Page::AppSettings,
            },
            Message::Home(m) => {
                self.home.update(Message::Home(m));
            }
            Message::WindowResized(w, h) => {
                // current page?
                match self.active_tab {
                    Page::Home => self.home.update(Message::WindowResized(w, h)),
                    _ => todo!(),
                    // Page::Edit => self.edit.update(Message::WindowResized(w, h)),
                    // Page::Practice => self.practice.update(Message::WindowResized(w, h)),
                    // Page::AppSettings => self.app_settings.update(Message::WindowResized(w, h)),
                };
            }
            _ => unreachable!(),
        }
        Command::none()
    }

    fn view(&self) -> Element<'_, Self::Message> {
        let page = match self.active_tab {
            Page::Home => self.home.view(),
            Page::Edit => self.edit.view(),
            Page::Practice => self.practice.view(),
            Page::AppSettings => self.app_settings.view(),
        };

        let picker = pick_list(&Page::ALL[..], Some(self.active_tab), Message::PageSelected);

        container(column(vec![
            row(vec![picker.into()]).padding(10).into(),
            page,
        ]))
        .width(Length::Fill)
        .height(Length::Fill)
        .into()
    }

    fn subscription(&self) -> Subscription<Message> {
        event::listen_with(|event, _| match event {
            Event::Window(window::Event::Resized { width, height }) => {
                Some(Message::WindowResized(width, height))
            }
            _ => None,
        })
    }
}
