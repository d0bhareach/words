// this module live only in one thread it do not 'know' about async code.
use super::database_actor::DatabaseActorMessageValue;
use super::entity::{Definition, Example, FullWord, FullWordExt, Word};
use crate::error::Error;

use rusqlite::{Connection, Statement};

pub struct DatabaseWorker<'conn> {
    get_all: Statement<'conn>,
    get_full_word_by_id: Statement<'conn>,
}

impl<'conn> DatabaseWorker<'conn> {
    // main end poit to actual databae operations
    pub fn do_work(
        &mut self,
        value: DatabaseActorMessageValue,
    ) -> Result<DatabaseWorkerResponse, Error> {
        match value {
            DatabaseActorMessageValue::GetAllWords => {
                Ok(DatabaseWorkerResponse::Words(self.get_all_words()?))
            }
            DatabaseActorMessageValue::GetFullWordById(id) => Ok(DatabaseWorkerResponse::FullWord(
                self.get_full_word_by_id(id)?,
            )),
        }
    }

    pub fn new(c: &'conn Connection) -> Self {
        Self {
            get_all: c
                .prepare("SELECT * FROM word")
                .expect("unable to prepare get all words statement"),
            get_full_word_by_id: c
                .prepare(
                    "SELECT word.*, definition.*, ex.* FROM word 
                    JOIN definition ON word.id=definition.word_id
                    JOIN (SELECT example.id, example.example FROM example
                    INNER JOIN word_example ON word_example.example_id=example.id
                    WHERE word_example.word_id=?1) as ex WHERE word.id=?1",
                )
                .expect("unable to prepare get full word by id"),
        }
    }
    // This functions could be private, but left then public for tests
    // low level sql operations
    pub fn get_all_words(&mut self) -> rusqlite::Result<Vec<Word>> {
        let rows = self.get_all.query_map([], |row| {
            Ok(Word {
                id: row.get(0)?,
                word: row.get(1)?,
                stamm: row.get(2).ok(),
                weight: row.get(3)?,
            })
        });
        let mut words: Vec<Word> = Vec::new();
        for w in rows?.flatten() {
            words.push(w);
        }

        Ok(words)
    }
    // in sqlite keys are i64
    // use mapped to map to tuples and then iter and fold.
    pub fn get_full_word_by_id(&mut self, id: i64) -> rusqlite::Result<FullWordExt> {
        let rows = self.get_full_word_by_id.query([&id])?;
        let mut res: FullWordExt = rows
            .mapped(|r| {
                let word_id = r.get(0).unwrap_or(0);
                let word_txt = r.get(1).unwrap_or("".to_string());
                let stamm = r.get(2).ok();
                let weight = r.get(3).unwrap_or_default();
                let word = Word::new(word_id, word_txt, stamm, weight);
                let definition_id = r.get(4).unwrap_or_default();
                let definition_word_id = r.get(5).unwrap_or_default();
                let definition_txt = r.get(6).unwrap_or_default();
                let info = r.get(7).ok();
                let form = r.get(8).ok();
                let definition = Definition::new(
                    definition_id,
                    definition_word_id,
                    definition_txt,
                    info,
                    form,
                );
                let example_id = r.get(9).unwrap_or_default();
                let example_txt = r.get(10).unwrap_or_default();
                let example = Example::new(example_id, example_txt);
                Ok(FullWord {
                    word,
                    definition,
                    example,
                })
            })
            .fold(FullWordExt::default(), |mut w, r| {
                let r = r.unwrap_or_default();
                w.word = r.word;
                w.definition.push(r.definition);
                w.example.push(r.example);
                w
            });
        res.definition.sort();
        res.definition.dedup();
        res.example.sort();
        res.example.dedup();
        Ok(res)
    }
}

#[derive(Debug)]
pub enum DatabaseWorkerResponse {
    Words(Vec<Word>),
    FullWord(FullWordExt),
}
/* ---------------------     Entities      ----------------------*/
// TODO: prepare entities for other tables too. Must be able to compare one of a kind to another.

// get allwords
// get all words like
// get one word  and definitions and examples. User cliced on word in dropdow list or insterted correct word
// what to do when user insert wrong word? - Search words similar to word.
// update word (time searched) and result if learned true if not learned false
// update word (user edit word in some way).
// delete word and associated data. (bad function but required).
// save word similar to update
