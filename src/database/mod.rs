pub mod create_table;
pub mod database_actor;
pub mod database_actor_handler;
pub mod database_worker;
pub mod entity;
pub mod insert_data;
pub use database_actor::{DatabaseActor, DatabaseActorMessage, DatabaseActorMessageValue};
pub use database_actor_handler::DatabaseActorHandler;
pub use database_worker::DatabaseWorker;

use crate::error::Error;
use rusqlite::Connection;
use std::path::{Path, PathBuf};

static DB_PATH: &str = "words/words-0.1.0";

fn data_path() -> Result<PathBuf, Error<'static>> {
    use directories_next::BaseDirs;
    if let Some(base_dirs) = BaseDirs::new() {
        let path = base_dirs.data_dir();
        let mut path = path.to_path_buf();
        path.push(DB_PATH);
        println!("{:?}", path);
        return Ok(path.to_path_buf());
    }
    Err(Error::CustomError("unable to get data path"))
}

fn database_existst(path: &Path) -> bool {
    path.is_absolute() && path.exists()
}

pub fn connection() -> Result<Connection, Error<'static>> {
    let path = data_path()?;
    let flag = database_existst(path.as_path());
    let dir_path = path
        .parent()
        .ok_or_else(|| Error::CustomError("unable to get parent of path:"))?;
    if !dir_path.exists() {
        std::fs::create_dir(dir_path)?;
    }

    let conn = Connection::open(path.as_path())?;
    if !flag {
        // must open connection and create tables
        create_table::create_tables(&conn).unwrap();
        insert_data::insert_values(&conn).unwrap();
    }
    Ok(conn)
}
