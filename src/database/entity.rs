#[derive(Debug, PartialEq, Clone)]
pub struct Word {
    pub id: i64,
    pub word: String,
    pub stamm: Option<String>,
    pub weight: f32,
}
impl Word {
    pub fn new(id: i64, word: String, stamm: Option<String>, weight: f32) -> Self {
        Self {
            id,
            word,
            stamm,
            weight,
        }
    }
}
impl std::default::Default for Word {
    fn default() -> Self {
        Self::new(0, "".to_owned(), None, 0.0)
    }
}

#[derive(Debug, PartialEq, PartialOrd, Ord, Eq, Clone)]
pub struct Definition {
    pub id: i64,
    pub word_id: i64,
    pub definition: String,
    pub info: Option<String>,
    pub form: Option<String>,
}

impl Definition {
    pub fn new(
        id: i64,
        word_id: i64,
        definition: String,
        info: Option<String>,
        form: Option<String>,
    ) -> Self {
        Self {
            id,
            word_id,
            definition,
            info,
            form,
        }
    }
}
impl std::default::Default for Definition {
    fn default() -> Self {
        Self::new(0, 0, "".to_owned(), None, None)
    }
}

#[derive(Debug, PartialEq, PartialOrd, Ord, Eq, Clone)]
pub struct Example {
    pub id: i64,
    pub example: String,
}
impl Example {
    pub fn new(id: i64, example: String) -> Self {
        Self { id, example }
    }
}
impl std::default::Default for Example {
    fn default() -> Self {
        Self::new(0, "".to_owned())
    }
}

#[derive(Debug, PartialEq)]
pub struct Collection {
    pub id: i64,
    pub collection: String,
}
impl Collection {
    pub fn new(id: i64, collection: String) -> Self {
        Self { id, collection }
    }
}
impl std::default::Default for Collection {
    fn default() -> Self {
        Self::new(0, "".to_owned())
    }
}

#[derive(Debug, Default)]
pub struct FullWord {
    pub word: Word,
    pub definition: Definition,
    pub example: Example,
}
// full text search with definitions and examples as vectors, because there could
// be many of these for one word
#[derive(Debug, Default, Clone)]
pub struct FullWordExt {
    pub word: Word,
    pub definition: Vec<Definition>,
    pub example: Vec<Example>,
}
