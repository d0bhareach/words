/*errichten
Krippe
versorgen
austreten
einziehen
Lohn
Besitz
Fürst
Verfassung
verklagen
Verwaltung
Gehalt
Pfarrer
Einnahme
abschaffen
taufen
Gebühr
beziehen
ereignen
Vorgang
*/
use crate::error::Error;
use rusqlite::Connection;

static INSERT_WORDS: &str = "
INSERT INTO word (word)
VALUES
(\"errichten\"),
(\"Krippe\"),
(\"versorgen\"),
(\"austreten\"),
(\"einziehen\"),
(\"Lohn\"),
(\"Besitz\"),
(\"Fürst\"),
(\"Verfassung\"),
(\"verklagen\"),
(\"Verwaltung\"),
(\"Gehalt\"),
(\"Pfarrer\"),
(\"Einnahme\"),
(\"abschaffen\"),
(\"taufen\"),
(\"Gebühr\"),
(\"beziehen\"),
(\"ereignen\"),
(\"Vorgang\");";

static INSERT_DEFINITION: &str = "
INSERT INTO definition (word_id, definition, info) VALUES
(1,\"1)строить, сооружать, воздвигать\n2)основывать, учреждать\n3)юр. составить (завещание и т. п.)\", \"vt\"),
(2, \"1)кормушка\n2)сокр. от Kinderkrippe ясли (детские)\",\"f <-, -n>\"),
(3, \"1) (mit D) снабжать, обеспечивать (кого-л, что-л чем-л)\n2) обслуживать (кого-л); ухаживать, заботиться (о ком-л, о чём-л)\", \"vt\"),
(4, \"| выходить (на открытое место, из чего-либо)\", \"vi (s)\"),
(4, \"|| 1) вытаптывать\n2)затаптывать, гасить ногами (огонь и т. п.)\", \"vt\"),
(5, \"| 1)втягивать, протягивать (что-л во что-л); вдевать, продевать (нитку)\n2) втягивать, вдыхать (воздух, аромат и т. п.)\n3) взыскивать, взимать, собирать (налоги и т. п.)\n 4) призывать на военную службу\n5) полигр. сдвигать, делать отступ\", \"vt\nzieht ein · zog ein · ist eingezogen\"),
(5, \"|| 1) въезжать, переезжать (в квартиру и т. п.)\n2) вступать, входить (в город и т. п.)\n3) наступать (о времени года)\n4) проникать (о жидкостях)\", \"vi(s)\nzieht ein · zog ein · ist eingezogen\"),
(6, \"1) зарплата, заработная плата (рабочих)\n2) тк sg вознаграждение, награда, премия\", \"m <-(e)s, Löhne>\"),
(7, \"1) имущество, собственность\n2) (G, von D) тк sg владение, обладание (чем-л)\", \"m <-es, -e>\"),
(8, \"1) князь (титул и обладатель этого титула)\n2) князь; монарх, государь (правитель какого-л гос-ва)\", \"m <-en, -en>\"),
(9, \"1) конституция; свод законов\n2) тк sg состояние (физическое, эмоциональное и т. п.), расположение духа\", \"f <-, -en>\"),
(10, \"1) предъявлять (судебный) иск (кому-л); подавать иск (в суд) (на кого-л)\n2) диал ябедничать, жаловаться (на кого-л)\", \"vt\n<du verklagst, er verklagte, hat verklagt>\"),
(11, \"администрация, управление\", \"f <-, -en>\"),
(12, \"| зарплата; оклад\", \"n  <-(e)s, Gehälter>\"),
(12, \"|| содержание (произведения, учения, книги и т. п.)\", \"m <-(e)s, -e>\"),
(13, \"пастор, (приходской) священник\", \"m <-s, ->\"),
(14, \"1) приём (лекарства, пищи)\n2) приход, поступление (денег), выручка\", \"f <-, -n>\"),
(15, \"отменять, упразднять, избавляться (от чего-л)\", \"vt\"),
(16, \"крести́ть\", \"vt\"),
(17, \"сбор,пошлина, плата\", \"f <-, -en>\"),
(18, \"1)обивать (напр мебель); покрывать (напр материей)\n2) въезжать (напр в квартиру)\n3) воен занимать (напр должность, позицию в вопросе и.т.п.)\n4) получать (товары по заказу)\n5) швейц взыскивать (деньги)\n6) (auf A) относить (к кому-л, чему-л)\n\nsich beziehen\n(auf A) ссылаться (на кого-л, что-л)\n(auf A) относиться (к кому-л, чему-л)\", \"vt\nbezieht - bezog - hat bezogen\"),
(19, \"происходи́ть\", \"VERB refl\"),
(20, \"1) процесс; течение (событий и т. п.)\n2) канц. дело, досье\", \"m <-(e)s, ..gänge>\");";

static INSERT_EXAMPLE: &str = "
INSERT INTO example (example) VALUES
(\"In den letzten Wochen hat sich viel ereignet\"),
(\"Die Demonstranten errichteten Barrikaden.\"),
(\"Auf diesem Gebiet wurde schließlich der neue Staat errichtet.\"),
(\"Sie bringt das Kind morgens in die/zur Krippe.\"),
(\"Im Winter füllt der Förster die Krippen für Rehe und Hirsche mit Heu.\"),
(\"Er hat die Firma auf Schadensersatz verklagt.\"),
(\" Der Pfarrer taufte das Baby auf den Namen Michael.\"),
(\"Er ließ sich taufen.\"),
(\"Sie haben ihren Hund auf den Namen Waldi getauft.\"),
(\"Die Miete wird monatlich eingezogen.\"),
(\" Der Fischer musste das Netz einziehen, ohne etwas gefangen zu haben.\"),
(\" Wie kam er in den Besitz der geheimen Dokumente?\"),
(\"Die Ware geht mit der Bezahlung in Ihren Besitz über.\"),
(\"Können Sie die Vorgänge in jener Nacht beschreiben/schildern?\"),
(\"Der Lehrer erläutert die chemischen Vorgänge bei der alkoholischen Gärung.\"),
(\"Die Einnahme der feindlichen Festung erwies sich als schwierig.\"),
(\"Die Einnahmen der Firma sind im letzten Jahr erheblich gestiegen.\");";

static INSERT_WORD_EXAMPLE: &str = "
INSERT INTO word_example(word_id, example_id) VALUES
(19, 1),
(1, 2),
(1, 3),
(2, 4),
(2, 5),
(10, 6),
(13, 7),
(16, 7),
(16, 8),
(16, 9),
(5, 10),
(5, 11),
(7, 12),
(7, 13),
(20, 14),
(20, 15),
(14, 16),
(14, 17);";

pub fn insert_values(connection: &Connection) -> Result<(), Error> {
    let mut command = String::from("BEGIN;");
    command.push_str(INSERT_WORDS);
    command.push_str(INSERT_EXAMPLE);
    command.push_str(INSERT_DEFINITION);
    command.push_str(INSERT_WORD_EXAMPLE);
    command.push_str("COMMIT;");
    connection.execute_batch(command.as_str())?;
    Ok(())
}
