// this module is intermediate between database and async code that is about to be used in iced
// it use DatabaseWorker to process sql and responce to DatabaseActorHandler.
use super::database_worker::DatabaseWorkerResponse;
use super::DatabaseWorker;
use oneshot::Sender as OneshotSender;
use std::sync::mpsc::Receiver;

// allow otherwise clippy complains about receiver and worker are never read
#[allow(dead_code)]
pub struct DatabaseActor<'conn> {
    receiver: Receiver<DatabaseActorMessage>,
    worker: DatabaseWorker<'conn>,
}

impl<'conn> DatabaseActor<'conn> {
    pub fn new(receiver: Receiver<DatabaseActorMessage>, worker: DatabaseWorker<'conn>) -> Self {
        Self { receiver, worker }
    }
    async fn handle_message(&mut self, msg: DatabaseActorMessage) {
        if let DatabaseActorMessage::Request { value, respond_to } = msg {
            let response = self.worker.do_work(value).expect("log error here");
            respond_to
                .send(DatabaseActorMessage::Response(response))
                .unwrap();
        }
    }

    pub fn run(&mut self) {
        while let Ok(msg) = self.receiver.recv() {
            let fut = async {
                let _ = self.handle_message(msg).await;
            };
        }
    }
}

pub enum DatabaseActorMessage {
    Request {
        value: DatabaseActorMessageValue,
        respond_to: OneshotSender<DatabaseActorMessage>,
    },
    Response(DatabaseWorkerResponse),
}

pub enum DatabaseActorMessageValue {
    GetAllWords,
    GetFullWordById(i64),
}
