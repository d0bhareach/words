use crate::error::Error;
use rusqlite::Connection;
/*
ID, INTEGER
Word itself, TEXT
type (Noun, Verb, Adjective is optional) INTEGER // would be enum
if word is noun there must be string with informaton TEXT
Word root  (Optional) TEXT
weight this is indicates how often word is shown REAL
*/
// TODO: Move stamm to separate table.
// TODO: need fts table to search definitions and search for words. Need it for back way search
// when I search for word RU -> DE
static CREATE_WORD_TABLE: &str = "
CREATE TABLE IF NOT EXISTS word (
id INTEGER PRIMARY KEY,
word TEXT NOT NULL,
stamm TEXT,
weight REAL DEFAULT 0);";

static CREATE_WORD_IDX: &str = "CREATE INDEX IF NOT EXISTS word_idx ON word(word);";
static CREATE_STAMM_IDX: &str = "CREATE INDEX IF NOT EXISTS stamm_idx ON word(stamm);";

/*
Need differrent table to store time when word started to appear in learning patthern
this table appear in calculations for words weight.
*/
static CREATE_WORD_TIME_TABLE: &str = "
CREATE TABLE IF NOT EXISTS time (
id INTEGER PRIMARY KEY,
word_id INTEGER NOT NULL,
time INTEGER NOT NULL DEFAULT (strftime('%s','now')),
result INTEGER DEFAULT 0,
FOREIGN KEY(word_id) REFERENCES word(id) ON DELETE CASCADE ON UPDATE CASCADE);";
static CREATE_TIME_WORD_ID_IDX: &str = "CREATE INDEX IF NOT EXISTS word_id_idx ON time(word_id);";

/*
ID,
definition, // maybe too make it JSON Array??
type - verb, noun, adjective?
defenition - actual definition,
info - other short information like for example plurals for nouns or tenses for verbs
form - json object with forms good for verbs, adjectives.
word_id foregn key
*/
static CREATE_DEFINITION_TABLE: &str = "
CREATE TABLE IF NOT EXISTS definition (
id INTEGER PRIMARY KEY,
word_id INTEGER NOT NULL,
definition TEXT,
info TEXT,
form TEXT default NULL,
FOREIGN KEY(word_id) REFERENCES word(id) ON DELETE CASCADE ON UPDATE CASCADE);";
// must help with joins.
static CREATE_DEFINITION_WORD_ID_IDX: &str =
    "CREATE INDEX IF NOT EXISTS word_id_idx ON definition(word_id);";

static CREATE_EXAMPLE_TABLE: &str = "
CREATE TABLE IF NOT EXISTS example (id INTEGER PRIMARY KEY, example TEXT);";
// must read more and try to work with FTS5 for this table.
// I need full text search when I will search for examples
// when word is added to dictionary this table may alredy contain examples for this word
static CREATE_EXAMPLE_FTS_TABLE: &str = "
CREATE VIRTUAL TABLE IF NOT EXISTS example_fts USING FTS5(example);";

/*
 an FTS5 table has an implicit INTEGER PRIMARY KEY field named rowid
 one word can have many examples, one example can be used for many words
 many-to-many
*/
static CREATE_WORD_EXAMPLE_TABLE: &str = "
CREATE TABLE IF NOT EXISTS word_example (
id INTEGER PRIMARY KEY,
word_id INTEGER NOT NULL,
example_id INTEGER NOT NULL,
FOREIGN KEY(word_id) REFERENCES word(id) ON DELETE SET NULL ON UPDATE CASCADE,
FOREIGN KEY(example_id) REFERENCES example(id) ON DELETE SET NULL ON UPDATE CASCADE);";

static CREATE_WORD_EXAMPLE_IDX_WORD_ID: &str =
    "CREATE INDEX IF NOT EXISTS example_word_idx ON word_example(word_id);";

static CREATE_WORD_EXAMPLE_IDX_EXAMPLE_ID: &str =
    "CREATE INDEX IF NOT EXISTS word_example_idx ON word_example(example_id);";

/*
Some words have with other word combining and different translation becamming.
Must be able to search on ru-de order. This table would not be used in search for words, but it must be
used in learning mode, especially in ru-de verb ziehen is an exellent example of how word can have many definitions
For now I think of using definition table for this.
Commented idiom table probable candidate for deletion.
*/
/*
static CREATE_IDIOM_TABLE: &str = "
CREATE TABLE IF NOT EXISTS idioms (
id INTEGER PRIMARY KEY,
word_id INTEGER NOT NULL,
idiom TEXT NOT NULL,
translation TEXT,
FOREIGN KEY(word_id) REFERENCES word(id) ON DELETE SET NULL ON UPDATE CASCADE);";
*/

// dropdown list of collections word can be in many collections
static CREATE_COLLECTION_TABLE: &str = "
CREATE TABLE IF NOT EXISTS collection (
id INTEGER PRIMARY KEY,
collection TEXT NOT NULL);";

// word can belong to many collections
// many-to-many
static CREATE_WORD_COLLECTION_TABLE: &str = "
CREATE TABLE IF NOT EXISTS word_collection (
id INTERGER PRIMARY KEY,
word_id INTEGER NOT NULL,
collection_id INTEGER NOT NULL,
FOREIGN KEY(word_id) REFERENCES word(id) ON DELETE SET NULL ON UPDATE CASCADE,
FOREIGN KEY(collection_id) REFERENCES collection(id) ON DELETE SET NULL ON UPDATE CASCADE);";

static CREATE_COLLECTION_ID_IDX: &str =
    "CREATE INDEX IF NOT EXISTS collection_id_idx ON word_collection(collection_id);";

pub fn create_tables(conn: &Connection) -> Result<(), Error> {
    let mut command = String::from("BEGIN;");
    command.push_str(CREATE_WORD_TABLE);
    command.push_str(CREATE_WORD_IDX);
    command.push_str(CREATE_STAMM_IDX);
    command.push_str(CREATE_WORD_TIME_TABLE);
    command.push_str(CREATE_TIME_WORD_ID_IDX);
    command.push_str(CREATE_DEFINITION_TABLE);
    command.push_str(CREATE_DEFINITION_WORD_ID_IDX);
    command.push_str(CREATE_EXAMPLE_TABLE);
    command.push_str(CREATE_EXAMPLE_FTS_TABLE);
    command.push_str(CREATE_WORD_EXAMPLE_TABLE);
    command.push_str(CREATE_WORD_EXAMPLE_IDX_WORD_ID);
    command.push_str(CREATE_WORD_EXAMPLE_IDX_EXAMPLE_ID);
    // command.push_str(CREATE_IDIOM_TABLE);
    command.push_str(CREATE_COLLECTION_TABLE);
    command.push_str(CREATE_WORD_COLLECTION_TABLE);
    command.push_str(CREATE_COLLECTION_ID_IDX);
    command.push_str("COMMIT;");
    // command = command.replace('\n', "");
    conn.execute_batch(command.as_str())?;
    Ok(())
}
