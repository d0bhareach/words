// This module is actuall database end point iced is going to use it must be possible to clone it.
// this module return actual data from database.
use super::{
    database_worker::DatabaseWorkerResponse, DatabaseActorMessage, DatabaseActorMessageValue,
};
use crate::error::Error;
use std::sync::mpsc::Sender;

#[derive(Clone, Debug)]
pub struct DatabaseActorHandler {
    sender: Sender<DatabaseActorMessage>,
}

impl DatabaseActorHandler {
    pub fn new(sender: Sender<DatabaseActorMessage>) -> Self {
        Self { sender }
    }
    async fn work(
        &mut self,
        value: DatabaseActorMessageValue,
    ) -> Result<DatabaseWorkerResponse, Error> {
        let (sender, receiver) = oneshot::channel();
        self.sender
            .send(DatabaseActorMessage::Request {
                value,
                respond_to: sender,
            })
            .unwrap();
        match receiver.recv() {
            Ok(DatabaseActorMessage::Response(res)) => Ok(res),
            Ok(DatabaseActorMessage::Request {
                value: _,
                respond_to: _,
            }) => Err(Error::CustomError(
                "get wrong type from receiver end in DatabaseActorHandler.work",
            )),
            Err(e) => Err(Error::CustomError(stringify!(
                "error unwraping reciever in DatabaseActorHandler.work",
                e
            ))),
        }
    }
}
