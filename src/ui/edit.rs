use crate::Message;
use iced::{
    widget::{column, container, text},
    Element, Length, Sandbox,
};
// this must be composition of messages of all pages.
#[derive(Debug, Clone)]
pub enum EditMessage {}

#[derive(Debug, Clone)]
pub struct Edit {}

impl Sandbox for Edit {
    type Message = Message;
    fn title(&self) -> String {
        String::from("Edit Page")
    }

    fn new() -> Self {
        Self {}
    }

    fn update(&mut self, message: Message) {
        todo!()
    }

    fn view(&self) -> iced::Element<Message> {
        let txt: Element<Message> = text("Edit page text holder").into();
        container(txt)
            .height(Length::Fill)
            .width(Length::Fill)
            .center_x()
            .center_y()
            .into()
    }
}
