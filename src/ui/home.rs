// TODO: wrap in responsive, try to use function
// different view for different screen sizes
// searches return none. What to do?
use crate::Message;
use iced::{
    alignment,
    widget::{
        button, column, container, horizontal_space, lazy, row, scrollable, text, text_input,
        Column, Row, Text,
    },
    Alignment, Element, Length, Padding, Renderer, Sandbox,
};
use words::database::entity::FullWordExt;
// this must be composition of messages of all pages.
#[derive(Debug, Clone)]
pub enum HomeMessage {
    SearchInputChanged(String),
    ClearSearchInput,
}

#[derive(Debug, Clone)]
pub struct Home {
    search_input: String,
    word: Option<FullWordExt>,
    window_size: (u32, u32),
}

impl Sandbox for Home {
    type Message = Message;

    fn title(&self) -> String {
        String::from("Home Page")
    }

    fn new() -> Self {
        Self {
            search_input: String::new(),
            word: None,
            window_size: (0, 0),
        }
    }

    fn update(&mut self, message: Message) {
        match message {
            Message::Home(HomeMessage::SearchInputChanged(input)) => self.search_input = input,
            Message::Home(HomeMessage::ClearSearchInput) => {
                if !self.search_input.is_empty() {
                    self.search_input = String::new()
                }
            }
            Message::WindowResized(w, h) => self.window_size = (w, h),
            _ => todo!(),
        }
    }

    fn view(&self) -> iced::Element<Message> {
        let horizontal_padding = |w: u32| {
            if self.window_size.0 < 600 {
                Length::Fixed(20.0)
            } else {
                Length::FillPortion(1)
            }
        };
        let search_row: Element<self::Message, Renderer> = row!(
            horizontal_space(horizontal_padding(self.window_size.0)),
            text_input("", &self.search_input)
                .width(Length::FillPortion(9))
                .font(iced::Font {
                    weight: iced::font::Weight::Medium,
                    ..iced::Font::default()
                })
                .size(30)
                .padding(10)
                .on_input(|s| Message::Home(HomeMessage::SearchInputChanged(s))),
            button(
                Text::new("X")
                    .size(30)
                    .horizontal_alignment(alignment::Horizontal::Center)
            )
            .padding(10)
            .width(Length::FillPortion(1))
            .on_press(Message::Home(HomeMessage::ClearSearchInput)),
            horizontal_space(horizontal_padding(self.window_size.0)),
        )
        // .spacing(20)
        .padding([10, 0])
        .into();

        // depending on word is found or not must show definitions or simple message
        // that workd is not found.
        let word_definition: Option<Element<self::Message, Renderer>> = if let Some(w) = &self.word
        {
            let defs: Column<'_, Message, Renderer> = column(
                w.definition
                    .iter()
                    .map(|d| row!(Text::new(&d.definition)).into())
                    .collect(),
            );
            let examples: Column<'_, Message, Renderer> = column(
                w.example
                    .iter()
                    .map(|e| row!(Text::new(&e.example)).into())
                    .collect(),
            );

            Some(column!(row!(Text::new(w.word.word.as_str())), defs, examples).into())
        } else {
            None
        };

        let mut content = column!(search_row);
        if let Some(definitions) = word_definition {
            content = content.push(definitions);
        }
        container(content)
            .height(Length::Fill)
            .width(Length::Fill)
            .center_x()
            .into()
    }
}
