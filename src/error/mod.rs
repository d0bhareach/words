// module to wrap all errors that can be returned by the library
#[derive(Debug, thiserror::Error)]
pub enum Error<'a> {
    #[error("{0}")]
    CustomError(&'a str),
    #[error("{0}")]
    SqlError(#[from] rusqlite::Error),
    #[error("{0}")]
    IO_Error(#[from] std::io::Error),
}
