# Agenda
## Prime Goal
To learn lexica of some language words must be learned. Goal of this application is to improve learnin
schema and use modern research results.    
Our ability to remember a piece of information depends critically on the number of times we have reviewed it, the temporal distribution of the reviews, and the time elapsed since the last review. I need to develop algorithm whose goal is to ensure that learners spend more (less) time working on forgotten (recalled) information.   
During studies of this topic I have met such properties of this algorithm as:   
* recall probability m(t) depends on the forgetting rate, n(t), and the time elapsed since the last review   
where t is time.
* Writing such algorithms is a matter of long research and experimentation. So my initial attempt to write such algorith must concentrate on getting all essential data. Which later will allowe me perform fine tuning and calculation or review rate.

### Forgetting rate
We can have time elapsed since start of studie number of correct answers, number of total answers and time elapsed between two last rewies. Time elapsed is playing some role when practising is  regular and without any serious disruption.   
Let's express recall rate as correct answers / total unswers   
Time elapsed between sessions increase probability of forgetting the item so at first if I save time as timestamp than I would probably convert the resulting difference to hours with modulus to 1 so reall effect would start from 2 or more hours passed since last review.
Another consideration is that remembering item after short period of time is not exactly the same as remembering it after a logn time.   
What about adding check for words that kind of promoted, bring them to the top of selection??   
## Index
* [App is active, search for word](#when-application-is-active)
* [App is not active, but daemon is runnin on the background](#application-is-running-as-a-daemon)
* [How words are organized.](#how-words-are-organized)
* [How to save a word](#how-to-save-a-word)
* [Words](#words_general)
* [Questions](#questions)
* [Minimum Viable Product (MVP)](#mvp)

For a long time I wanted to make an application to work on my dictionary.
What I want to acheave:
### When application is active
but,not on foreground if I click a word this word
must to be searched in dictionary and defenition must be shown.    
1. If a word is present must encrease it's 'popularity' this means it will come
    more often when I going to study words.
2. When word is not present must ask for this word to be added to dictionary.    
        -  Must think how to add a sentence for examples at this stage.   
3. Application must work as dictionary and a training app. From time to time it must
make me learning words.
4. Add searching ability to the App. Search bar and autocompletion.
</br>[back to Index](#index)

### Application is running as a daemon.
The word must be added and marked as pending. It must be some indiacation of how many 
words are pending. Before it added to dictionanry it must already have 'popularity'.
This way I can organize more popular words and work on them before all others.
* What to do when a word is some form? I think when words are created there must be 
some special sort of word. Words that are some form of other word must have link to 
main word. Otherwise this word must be the same as others, when user clicks on this
word 'popularity' of this word should be increased. This must to be done automatically
when word is created all forms are added and if there are forms for the Word that
word's form must be added to dictionary automatically.
* What if word is already in the dict? I think his 'popularity' must be increased.
* How I would know when to add word, say I work and copy something? The World is not perferct, so better think on either settings or simple menu to switch on and off.
In settings it could be done like if words are selected in VS Code for example 
than do nothing.
</br>[back to Index](#index)

### How words are organized.
At the moment I concentrate myself with German language.
The Language has some properties that are not present in other languages. Looks 
like it will require different window for different word must to think about it.     
1. Nouns must have forms and declantion. Must think of color scheme and how to represent forms of the word. 
2. Verbs have tenses and declanations as well.
3. Words can have idioms that must to be learned as well.
4. It's better to learn in context, so must have examples for the word.
5. I think I must include tables and some grammar information at least for big themes 
like modal verbs, tenses, prepositions(?). This is not immediate need, though.
6. Show how many words are saved all togeter.
7. Must have groups Orientierung, Lebensmittel, Wohnung (etw). Should be possible to
leart words in groups as well.
8. Must be possible to select word inside Application and jump to words, defenition.
9. Must be possible to bind words together. Similar words for example.
10. Synonims.
11. The more information the better. All information about the word. Must look in
other dictionaries and have a look how it's done there.
</br>[back to Index](#index)


### How to save a word.
And how to connect them. Plural forms of Nouns for example where to keep it? Thogeher with a word or as a separate entry? For now I think separate entry is most preferable way, But with examples, or show examples when follow the link? Like: Äpfel - pl. Apfel -> go to devenition and automatically show
usage for plural form.    
When adding a word it's about to be some dounting task. Good thing this must to be 
done only once. Bad thing that it must to be done by hands. Must think **really** hard
on learning algorithm to make it worth. Must make app useful.    
It must be possible to change word.  
Wordstamm how to calculate it? Need it for async search.    
Verbforms too many repetitions, how to make them automatically?  
Must find way to connect verb forms with main definition.   
Word must have weight. When adding word weight must possible to assign. For words that I would like to learn before other ones.   
</br>[back to Index](#index)

### Words 
</br>[back to Index](#index)
<detais>
<summary id="words_general"></summary>
<p>Must make word creation and edition as automatic as possible. Check APIs. So far I have only wound
<a href="https://en.pons.com/open_dict/public_api/new">API for Pons.com</a>
check documantation there. At the very least I need automaic assistence when creating verbs, with
verbs forms. Also must look for better translations so far the best translation I have found is 
Lingvo Android App. In general it's possible to work around, but this way word creation would become
a dounting task.</p>
</br><a href="#index">[back to Index]</a>
</details>

 <details>
 <summary id="questions">Questions</summary>
 <ul>
    <li> I know that there is show / hide mechanism in gtk. Does GTK has z-index?</li>
    <li> How and where to keep App's State?  State management in GTK.</li>
    <li> GTK App on the background. How to mangage daemons in GTK. Say app is in background how do I bring it up and gain focus?
    <ol>
        <li><span style="font-size: 88%; color: #70dbdb">Tried to use visibility and listen to key event Ctrl-C when applicaion is not
    visible it did't work.</span></li>
    </ol>
    For now looks like closest thing I can find is listen to Clipboard Changes run it
    on background and react to clipboard changes accordingly.     
    <span style="color: green"> Must write simple App to hide it or minimize, run loop for Clipboard, bring App in focus when clipboard Event occured. </span></li>
    <li> How GTK Applications are build and installed? Especially with gtk4-rs?</li> 
    <li> It's a bit off question, but what applications are installed on Ubuntu
    by default which I don't use? is it save to remove them? And how?</li>
    <li> It's slyghtly too late, but what is the preferred way of creating applications for Ubuntu? Qt or gtk or whatever else?</li>
    <li> Gnome Builder Install or not?  And the unswer - don't. Doen't have good recommendations.</li>
 </ul>
</br><a href="#index">[back to Index]</a>
 </details>

 
 <details>
 <summary id="mvp">MVP (Minimum Viable Product)</summary>
 <p><b>CONCENTRATE ON FUNCTIONALITY NOT GUI!</b>Let GUI be as horrible as it can be.</p>
 <p>It looks like I stuck with this project. And worst I must make progress with my
 other things. So instead of making this Application ready from the first time I 
 would rather make Application which works and evolve from that one.
 There is a list of what I would like to work for this version:</p>
 <ul>
    <li>Word searching window.</li>
    <li>Mechanism to add word for the first time.</li>
    <li>Word saving Window. For all types of words Nouns, Verbs, Adjectives etc..</li>
    <li>Word searching and updating mechanism. If later I need to make changes must be able to make it.</li>
    <li>Algorithm for studying words. This one is a tough one. But this is actually the main reasong of this application.</li>
    <li>App istallation scripts.</li>
    <li>Minimum documentation.</li>
    <li>Just German language for now.</li>
    <li></li>
 </ul>
 <h3>Application Pages</h3>
 <ui>
    <li>Main. One where words are searched.</li>
    <li>List of all words without definitions. How to find them?</li>
    <li>Save/edit word page.</li>
    <li>Learning window. the one where word from algoritmically picked list are learned.</li>
    <li>Settings</li>
 </ui>
</br><a href="#index">[back to Index]</a>
 </details>

