## Database

I would like to use SurrealDb with RocksDb backend for on disk storage.
As of 10.01.24 I decided Sqlite will be used for backend.

Must really quickly learn about this database. What I would like to have.
* Search for exact word and search for similar words. 
* Must probably have separate tables for examples. Because example can already contain
the word searched word.
* Words have different information Nouns are not the same as Verbs.
* Words idioms and words that commonly used togeter, useful phrases and must be 
separate thing and also have qualities like words for learning.

### Words how they may look
#### table 'word'
this must be simple index for other table 'devenition'.
To search for a word especially for many verbs it's better to have word root. words are saved in lowercase. If word is noun first letter will be capitalized on the fly. Word can belong to many Collections
``` json
{
    word: "Word itself",
    info: {
        type: "Noun" | "Verb" | "Adjective",
        txt: "Other Information relative to word. Can be different for different types"
    }
    # may think to move defenition to separate table. 
    defenitions: [{
        info: "informaton for word defenition"
        text: "test; with words; defenition"
    }],
    synonims: [ # must be links to word table ]
    scope: 0.3232 # How often will it be shown.
}
```

#### table 'defenition'
Table to keep definitions for word. Fields:
type of word (verb, noun,..), translation, forms, сравнительные и превосходные степени. Verb forms can have similarities and several pronomen can have the same form.  Some forms are exactly the same for all pronomens.
[site to check for verb forms](https://konjugator.reverso.net/konjugation-deutsch-verb-abbei%C3%9Fen.html)

```bash

```
How to style translation

#### table 'forms'
forms can be plural, decinations, temporal perhaps will have to make 
separate table for each. For some verbs it's possible to have some sort of
algorithm to create forms 'on the fly'.

#### table defenition-example (many-to-many)

#### table 'example'
Example can be used for many other words. This way will save a little space and will get better citation for each example. And example can be used as a fts5 'full text search'.
``` json
{
    example: "bla bla",
    translation: "перевод примера."
}
```

#### table 'idom'

``` json 
{
    idiom: "Idiom with some words",
    translation: "Перевод Идиоматиоческого выражения",
    score: 0.98     # как часто всплывать в обучении.
}
```
