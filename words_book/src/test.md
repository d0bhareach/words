# SQLite tests
SQLite do not support mutithreaded connection. Tests that performed for database must
be carried out in single thread.   
Use `cargo test -- --test-threads=1` it's IMPORTANT!! Because for the tests test suite creates single thread_local rusqlite::Connection.    
It is really inconvenient to run sql search and join tests for the first time. Need to create sql scripts for creating and populating databases. This scripts shall follow the same operations as in rust code.      