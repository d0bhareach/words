select word.*, group_concat(definition_object, "**"), group_concat(res, "^^") from word
join (select json_object('id', d.id, 'word_id', d.word_id, 'definition',d.definition) as definition_object from definition d where word_id = 5)
on definition.word_id = word.id
JOIN (SELECT json_object('example_id',example.id, 'example', example.example) as res FROM example INNER JOIN word_example ON word_example.example_id=example.id
WHERE word_example.word_id = 5)
where word.id = 5 GROUP BY definition_object, res;