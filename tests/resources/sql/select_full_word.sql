SELECT word.*, definition.*, ex.* FROM word 
JOIN definition ON word.id=definition.word_id
JOIN (SELECT example.id, example.example FROM example INNER JOIN word_example ON word_example.example_id=example.id
WHERE word_example.word_id = 5) as ex WHERE word.id=5;