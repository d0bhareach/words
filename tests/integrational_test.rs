mod util;

use words::database::entity::Word;
use words::database::DatabaseWorker;

#[test]
fn my_runner() {
    util::init();
    util::CONN.with(|c| {
        let mut db = DatabaseWorker::new(c);
        a_test(&mut db);
        b_test();
        ac_test();
        d_test(&mut db);
    });
}

fn a_test(db: &mut DatabaseWorker) {
    let words = db.get_all_words().expect("can not get vec of all words");
    println!("{words:?}");
    assert!(words.len().eq(&20usize));
}

fn d_test(db: &mut DatabaseWorker) {
    let word = db.get_full_word_by_id(5).expect("can't get full word");
    println!("{word:?}");
}

fn b_test() {
    let w1 = Word::new(1, "w1".to_string(), None, 0.0);
    let mut w2 = Word::new(1, "w1".to_string(), None, 0.0);
    assert!(w1.eq(&w2));
    w2.id = 2;
    assert!(!w1.eq(&w2));
}

fn ac_test() {
    dbg!("ac_test");
    assert!(1.eq(&1));
}
