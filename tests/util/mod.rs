use rusqlite::Connection;
use words::database::create_table::create_tables;
use words::database::insert_data::insert_values;
thread_local! {
    pub static CONN: Connection =
        Connection::open_in_memory().expect("error open in memory connection");
}

pub fn init() {
    CONN.with(|conn| {
        create_tables(conn).unwrap();
        insert_values(conn).unwrap();
    })
}
